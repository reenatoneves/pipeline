package br.com.tecnisys.pipeline;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class SomaEndpoint {

    @GetMapping("/somar/{n1}/{n2}")
    public Resultado somar(@PathVariable Double n1, @PathVariable  Double n2){
        Resultado resultado = new Resultado();
        resultado.setN1(n1);
        resultado.setN2(n2);
        resultado.setSoma(n1 + n2);
        return resultado;
    }
}
